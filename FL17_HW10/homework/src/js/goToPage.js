function goToAddingPage() {
    location.hash = '#/Add';
}

function goToEditingPage(e) {   
    location.hash = `#/edit/:${e.target.dataset.id}`;   
}

function goToLikedPage() {
    location.hash = '#/liked';
}

function goBack() {
    history.back();
}

export {goToAddingPage, goToEditingPage, goToLikedPage, goBack};