import { renderPage, showMessage} from './app';

function getTweetArray() {
    let tweetArray = [];
    for (let i = 0; i < localStorage.length; i++) {
        let key = localStorage.key(i);        
        let tweetObject = JSON.parse( localStorage.getItem(key) );
        tweetArray.push(tweetObject);
    }        
        tweetArray.sort((a, b) => a.id - b.id);
        
        return tweetArray;
}

function findNextId() {
    let tweetArray = getTweetArray();
    let maxIdNumber = tweetArray.reduce((maxId, currentTweet) => {
        return +maxId > +currentTweet.id ? +maxId: +currentTweet.id;
    }, 0)
    return maxIdNumber + 1;
}

function isThereLiked() {
    let tweetArray = getTweetArray();
    return tweetArray.some( element => element.liked);    
}

function isTrehSuchTweet(tweet) {
    let tweetArray = getTweetArray();
    return tweetArray.some( element => element.text === tweet); 
}

function removeTweet(e) {
    let id = e.target.dataset.id;
    localStorage.removeItem(id);
    showMessage(`You've just removed tweet with id ${id}!`);     
    renderPage();   
}

function likeTweet(e) {    
    let id = e.target.dataset.id;
    let tweetObject = JSON.parse( localStorage.getItem(id) );
    if (e.target.dataset.liked === 'true') {
        tweetObject.liked = false;
        e.target.innerText = 'Like';
        showMessage(`Sorry you no longer like tweet with id ${id}!`);         
    } else {
        tweetObject.liked = true;
        e.target.innerText = 'Unlike';
        showMessage(`Hooray! You liked tweet with id ${id}!`);
    }
    e.target.dataset.liked = tweetObject.liked;
    let parsedTweet = JSON.stringify(tweetObject);
    localStorage.setItem(id, parsedTweet);
    
    renderPage();
    }

function clearTimeoutQueue(timeOutIDs) {
    timeOutIDs.forEach(id => {
        clearTimeout(id);
    })
    timeOutIDs = [];
}

export { getTweetArray, findNextId, isThereLiked, isTrehSuchTweet, removeTweet, likeTweet, clearTimeoutQueue };

