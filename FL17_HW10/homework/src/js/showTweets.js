import { appendMyElement } from './appendMyElement';
import { getTweetArray, removeTweet, likeTweet } from './supportFunctions';
import { goToEditingPage} from './goToPage';


let tweetItems = document.querySelector('#tweetItems');
const delay = 10;

function showTweets(onlyliked) {
    setTimeout(() => {
        tweetItems.lastElementChild.remove();    
        let tweetList = appendMyElement(tweetItems, 'ul', null, {class: 'tweet-list'});
        let tweetArray = getTweetArray();
        
        if (onlyliked) {            
            tweetArray = tweetArray.filter((element) => element.liked);
        }
    
        tweetArray.forEach((element) => {
            let tweet = appendMyElement(tweetList, 'li', null,
            {class: 'tweet-list-item'})
           let tweetText = appendMyElement(tweet, 'p', element.text,
            {class: element.liked ? 'tweet-paragraph liked' : 'tweet-paragraph',
             'data-id': element.id, 'data-liked': element.liked});
            tweetText.addEventListener('click', goToEditingPage);
           let removeButton = appendMyElement(tweet, 'button', 'Remove',
            {class: 'remove-button button', 'data-id': element.id, 'data-liked': element.liked});
            removeButton.addEventListener('click', removeTweet);
           let likeButton = appendMyElement(tweet, 'button',
           element.liked ? 'Unlike': 'Like',
            {class: 'like-button button', 'data-id': element.id, 'data-liked': element.liked});
           likeButton.addEventListener('click', likeTweet);  
        })        
    }, delay);   
}

export {showTweets};