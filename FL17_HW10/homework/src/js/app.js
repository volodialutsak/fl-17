import '../scss/styles.scss';
import { appendMyElement } from './appendMyElement';
import { createMyElement } from './createMyElement';
import { goToAddingPage, goToLikedPage, goBack } from './goToPage';
import { findNextId, isThereLiked, isTrehSuchTweet, clearTimeoutQueue } from './supportFunctions';
import { showTweets } from './showTweets';

let textArea = document.querySelector('#modifyItemInput');
    textArea.rows = 7;
    textArea.cols = 20;
    textArea.maxLength = '140';
document.querySelector('.formButtons');
let addTweetButton = document.querySelector('#addTweet');
let listPage = document.querySelector('.listPage');
let modifyItem = document.querySelector('#modifyItem');
let modifyItemHeader = document.querySelector('#modifyItemHeader');
let addTweetHeader = createMyElement( 'h1', 'Add tweet');
let saveButton = document.querySelector('#saveModifiedItem');
let cancelButton = document.querySelector('#cancelModification');
    addTweetHeader.style.display = 'none';
    modifyItem.prepend(addTweetHeader);

let navigationButtons = document.querySelector('#navigationButtons');
let messageBox = document.querySelector('#alertMessage');

let goToLikedButton = appendMyElement(navigationButtons, 'button', 'Go to liked',
 {class: 'go-to-liked-button button'});
let backButton = appendMyElement(navigationButtons, 'button', 'Back',
 {class: 'back-button buuton'});

const messageTime = 2000;
const timeOutIDs = [];

window.addEventListener('hashchange', renderPage);
window.addEventListener('load', renderPage )
addTweetButton.addEventListener('click', goToAddingPage);
saveButton.addEventListener('click', saveText);
cancelButton.addEventListener('click', cancel);
goToLikedButton.addEventListener('click', goToLikedPage);
backButton.addEventListener('click', goBack);

export function renderPage() {    
    if (location.hash === '#/Add') {
        listPage.style.display = 'none';
        addTweetHeader.style.display = 'block';
        modifyItem.style.display = 'block';
        modifyItemHeader.style.display = 'none';
    } else if (location.hash === '#/liked') {
        if(!isThereLiked()) {
            history.back();
        }
        listPage.style.display = 'block';
        addTweetHeader.style.display = 'none';
        modifyItem.style.display = 'none';
        goToLikedButton.style.display = 'none';        
        backButton.style.display = 'inline';
        addTweetButton.style.display = 'none';  
        showTweets(true);
    } else if (location.hash === '') {
        if (isThereLiked()) {
            goToLikedButton.style.display = 'inline';
        } else {
            goToLikedButton.style.display = 'none'; 
        }
        addTweetButton.style.display = 'inline';
        backButton.style.display = 'none';
        listPage.style.display = 'block';
        addTweetHeader.style.display = 'none';
        modifyItem.style.display = 'none';
        showTweets(false);
    } else {
        listPage.style.display = 'none';
        addTweetHeader.style.display = 'block';
        modifyItem.style.display = 'block';
        modifyItemHeader.style.display = 'none';
        let id = location.hash.match(/[0-9]+/g);
        let tweetObject = JSON.parse( localStorage.getItem(id) );
        textArea.value = tweetObject.text;
        saveButton.dataset.id = id;
    }
}

export function showMessage(message) {    
    messageBox.firstElementChild.innerText = message;
    messageBox.style.visibility = 'visible';
    clearTimeoutQueue(timeOutIDs);
    let timeOutId = setTimeout( () => {
        messageBox.style.visibility = 'hidden';
    }, messageTime);
    timeOutIDs.push(timeOutId);
    console.log(timeOutId);
}

function saveText() {    
    if(textArea.value.trim() !== ''){
        if (isTrehSuchTweet(textArea.value.trim())){
            showMessage('Error! You can\'t tweet about that');
        } else {
            if (location.hash === '#/Add') {
                let id = findNextId();                
                let parsedTweet = JSON.stringify({id: id, text: textArea.value, liked: false});
               
                localStorage.setItem(id, parsedTweet);
                textArea.value = '';
                history.back();                
            } else {
                let id = saveButton.dataset.id;
                let tweetObject = JSON.parse( localStorage.getItem(id) );
                let liked = tweetObject.liked;
    
                let parsedTweet = JSON.stringify({id: id, text: textArea.value, liked: liked});
                localStorage.setItem(id, parsedTweet);
                textArea.value = '';
                history.back();                
            }
            messageBox.style.visibility = 'hidden';
        }        
    }    
}

function cancel() {
    textArea.value = '';
    history.back();  
    messageBox.style.visibility = 'hidden';
}