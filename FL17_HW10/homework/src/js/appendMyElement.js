import { createMyElement } from './createMyElement';

function appendMyElement(targetElement, tag, content, attributes) {
    let element = createMyElement(tag, content, attributes);
    targetElement.append(element);    
    return element;
}

export {appendMyElement};