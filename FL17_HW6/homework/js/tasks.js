function getMaxEvenElement(arr) {
    const evenDivider = 2;
    arr = arr.map(item => +item);

    let maxEven = arr.find(elem => !(elem%evenDivider));
        
    maxEven = arr.reduce((maxEvenElement, currentElement) => {
        if (!(currentElement%evenDivider)) {
            return Math.max(maxEvenElement, currentElement);
        } else {
            return maxEvenElement;
        }
    }, maxEven);

    return maxEven;
}

let a = 3;
let b = 5;
[a, b] = [b, a];
console.log(a);
console.log(b);

function getValue(value) {
    return value ?? '-';
}

function getObjFromArray(arr) {
    let obj = {};
    for(let elem of arr) {
    let [key, value] = elem;
        if (Array.isArray(value)) {
            value = getObjFromArray(value);
        }
        obj[key] = value;
    }
    return obj;
}

function addUniqueId(obj) {
    let resultObj = JSON.parse(JSON.stringify(obj));
    resultObj.id = Symbol();
    return resultObj;
}

function getRegroupedObject({name: firstName, detailes: {id, age, university}}) {
    return {
        university,
        user: {
            age,
            firstName,
            id
        }
    };
}

function getArrayWithUniqueElements(arr) {    
   return Array.from(new Set(arr));
}

function hideNumber(number) {
    const unhidenDigits = 4;
    let lastFourDigits = number.slice(-unhidenDigits);
    return lastFourDigits.padStart(number.length, '*');
}

function add(a = required('a'), b = required('b')) {
    return a + b;
    
}

function required(parameter) {
    throw new Error(parameter + ' is required');
}

function* generateIterableSequence() {
    yield 'I';
    yield 'love';
    yield 'EPAM';
}