class BirthdayService {
    constructor() {
        this.now = new Date()
    }

    howLongToMyBirthday(...date) {
        let promise = new Promise((resolve, reject) => {
            if (isValidDate(...date)) {
                setTimeout(() => resolve(new Date(date)), 100)
            } else {
                setTimeout(() => reject(Error('Wrong argument!')), 100);
            }
        });

        return promise.then(data => {
            let closestBirthday;
            const nearBirthdays = [];

            let thisYear = this.now.getFullYear();
            let thisMonth = this.now.getMonth();
            let thisDate = this.now.getDate();
            let birthdayMonth = data.getMonth();
            let birthdayDate = data.getDate();

            nearBirthdays.push( {year: 'last', date: new Date(thisYear - 1, birthdayMonth, birthdayDate)} );
            nearBirthdays.push( {year: 'current', date: new Date(thisYear, birthdayMonth, birthdayDate)} );
            nearBirthdays.push( {year: 'next', date: new Date(thisYear + 1, birthdayMonth, birthdayDate)} );
            
            nearBirthdays.forEach(item => {
                item.diff = item.date - this.now;
            })

            closestBirthday = nearBirthdays[0];

            nearBirthdays.forEach(item => {
                if (Math.abs(item.diff) < Math.abs(closestBirthday.diff)) {
                    closestBirthday = item;
                }
            })

            let days = Math.ceil(closestBirthday.diff/(1000*3600*24));

            if( thisMonth === birthdayMonth && thisDate === birthdayDate) {
                return this.congratulateWithBirthday();
            } else {
                return this.notifyWaitingTime(days);
            }
            
        }).catch(error => error.message);
    }

    congratulateWithBirthday() {
        return log('Hooray!!! It is today!');
    }

    notifyWaitingTime(waitingTime) {
        if (waitingTime > 0) {
            return log(`Soon...Please, wait just ${waitingTime} day/days`);
        } else {
            return log(`Oh, you have celebrated it ${Math.abs(waitingTime)} day/s ago, don't you remember?`);
        }
    }
}

function isValidDate(date) {
    if(Date.parse(date)) {
        return true;
    } else {
        return false;
    }    
}

function log(message) {
    console.log(message);
    return message;
}

module.exports = BirthdayService;
