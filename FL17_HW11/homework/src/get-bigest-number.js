const getBigestNumber = (...args) => {    
    const minArgsNumber = 2;
    const maxArgsNumber = 10;
    if (args.length < minArgsNumber) {
        throw Error('Not enough arguments');
    } else if (args.length > maxArgsNumber) {
        throw Error('Too many arguments');
    }
    for (let item of args) {
        if (!isNumber(item)) {
            throw Error('Wrong argument type');
        }
    }
    return Math.max(...args);
}

const isNumber = function(arg) {
    return typeof arg === 'number' && isFinite(arg);
}

module.exports = getBigestNumber;