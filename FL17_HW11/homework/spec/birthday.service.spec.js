const BirthdayService = require('../src/birthday.service');

describe('BirthdayService class functionality', () => {
    let promise;
    beforeEach( (done) => {
        let birthdayObj = new BirthdayService();
        promise = birthdayObj.howLongToMyBirthday('sdsfdad');
        done();
    });

    it('Function throws error when an argument can not be converted to Date()', () => {        
        promise.then((res) => {
            expect(res).toEqual('Wrong argument!');
        })       
    });
    

})