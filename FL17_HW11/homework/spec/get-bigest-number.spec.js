const getBigestNumber = require('../src/get-bigest-number');

describe('Getting biggest number functionality', () => {
    it('Function returns the biggest value', () => {
        expect(getBigestNumber(-2, 3, 4)).toBe(4);
    });

    it('Function throws error when an argument is not a number', () => {
        expect(() => getBigestNumber(-2, 3, 0.4, 4, '5')).toThrowError(Error, 'Wrong argument type');
    });

    it('Function throws error if there are less than 2 arguments', () => {
       expect(() => getBigestNumber(1)).toThrowError(Error, 'Not enough arguments');
    });

    it('Function throws error if there are more than 10 arguments', () => {
        expect(() => getBigestNumber(1,2,3,4,5,6,7,8,9,10,11)).toThrowError(Error, 'Too many arguments');
     });
});