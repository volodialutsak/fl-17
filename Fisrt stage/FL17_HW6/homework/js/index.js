function visitLink(path) {
	let storageItem = +localStorage.getItem(path);
	if (storageItem) {
		localStorage.setItem(path, ++storageItem);
	} else {
		localStorage.setItem(path, '1');
	}
}

function viewResults() {
	let ul = document.createElement('ul');

	let keyArray = [];
	for (let i = 0; i < localStorage.length; i++) {
		keyArray.push( localStorage.key(i) );
	}
	keyArray.sort().forEach((key) => {
		let li = document.createElement('li');
		li.append(`You visited ${key} ${localStorage.getItem(key)} time(s)`);
		ul.append(li);
	});

	localStorage.clear();
	document.body.firstElementChild.firstElementChild.append(ul);
}