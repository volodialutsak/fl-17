const appRoot = document.getElementById('app-root');

function createMyElement(tag, content, attributes) {
    let element = document.createElement(tag);
    if (attributes) {
        for (const [key, value] of Object.entries(attributes)) {
            element.setAttribute(key, value);
        }
    }
    
    if (content || content === 0 || /^\s$/.test(content)){
        element.append(content);
    }        
    return element;
}

function appendMyElement(targetElement, tag, content, attributes) {
    let element = createMyElement(tag, content, attributes);
    targetElement.append(element);    
    return element;
}

let clickAnimation = appendMyElement(appRoot, 'div', null, {class: 'click-animation'} )
clickAnimation.style.display = 'none';
document.body.addEventListener('click', animateClick, false);

function animateClick(event) {
    clickAnimation.style.position = 'absolute';
    clickAnimation.style.top = event.pageY - '25' +'px';
    clickAnimation.style.left = event.pageX - '7' +'px';
    clickAnimation.style.display = 'block';
    setTimeout(() => {
        clickAnimation.style.display = 'none';
    }, '300');
}

let h1 = appendMyElement(appRoot, 'h1', 'Contries search');
let form = appendMyElement(appRoot, 'form', null, {action: ''});

    let divRadioMessage = appendMyElement(form, 'div', null, {class: 'search'});

        let paragraphRadio = appendMyElement(divRadioMessage, 'p', 'Please choose the type of search:');
        
    let divRadio = appendMyElement(form, 'div', null, {class: 'search'});
   
        let ulRadio = appendMyElement(divRadio, 'ul');
        
            let li1 = appendMyElement(ulRadio, 'li');
            
                let radioInputByRegion = appendMyElement(li1, 'input', null,
                    {type: 'radio', name: 'radio-button', id: 'radio-by-region', class: 'pointer'});
                
                let lebelByRegion = appendMyElement(li1, 'label', 'By Region',
                    {for: 'radio-by-region', class: 'pointer'});

            let li2 = appendMyElement(ulRadio, 'li');
            
                let radioInputByLanguage = appendMyElement(li2, 'input', null,
                    {type: 'radio', name: 'radio-button', id: 'radio-by-language', class: 'pointer'});
                
                let lebelByLanguage = appendMyElement(li2, 'label', 'By Language',
                 {for: 'radio-by-language', class: 'pointer'});
                
    let divSelect = appendMyElement(form, 'div', null, {class: 'select-div'});
    
        let lebelForSelect = appendMyElement(divSelect, 'label', 'Please choose search query:',
            {for: 'item-list'});
        
        let selectListArray = [];
        renderSelectList();

let missingData = appendMyElement(appRoot, 'p', 'No items, please choose search query');
missingData.style.display = 'none';

let table = appendMyElement(appRoot, 'table', null, {class: 'country-table'});
table.style.display = 'none';

    appendMyElement(table, 'col', null, {span: 3, class: 'col-1-3'});    
    appendMyElement(table, 'col', null, {span: 1, class: 'col-4'});
    appendMyElement(table, 'col', null, {span: 2, class: 'col-5-6'});

    let thead = appendMyElement(table, 'thead');
    
        let trHead = appendMyElement(thead, 'tr');
        
            let thCountryName = appendMyElement(trHead, 'th', 'Country name');
            
                let countrySortingArrow = appendMyElement(thCountryName, 'span', null,
                    {class: 'sort-by-country pointer', arrow: 'up'});                
                countrySortingArrow.innerHTML = '&#8593;';
                countrySortingArrow.addEventListener('click', changeSorting, false);
              
            let thCapital = appendMyElement(trHead, 'th', 'Capital');
            
            let thWorldRegion = appendMyElement(trHead, 'th', 'World region');
            
            let thLanguages = appendMyElement(trHead, 'th', 'Languages');
            
            let thArea = appendMyElement(trHead, 'th', 'Area');

                let areaSortingArrow = appendMyElement(thArea, 'span', null,
                    {class: 'sort-by-area pointer', arrow: 'up-down'});                
                areaSortingArrow.innerHTML = '&#8597;';
                areaSortingArrow.addEventListener('click', changeSorting, false);
               
            let thFlag = appendMyElement(trHead, 'th', 'Flag');
            
        let tbody = appendMyElement(table, 'tbody', null, {class: 'table-body'});
       
let countryArrayToShow = [];

ulRadio.addEventListener('change', changedRadioButton, false);
ulRadio.addEventListener('change', activateSearch, false);

function changeSorting(event) {
    let targetArrow = event.target;
    let targetArrowAttr = targetArrow.attributes.getNamedItem('arrow')

    if (targetArrowAttr.value === 'up') {
        targetArrowAttr.value = 'down';
        targetArrow.innerHTML = '&#8595;';
        renderResult();
    } else if (targetArrowAttr.value === 'down') {
        targetArrowAttr.value = 'up-down';
        targetArrow.innerHTML = '&#8597;';
    } else if (targetArrowAttr.value === 'up-down') {
        targetArrowAttr.value = 'up';
        targetArrow.innerHTML = '&#8593;';
        if (targetArrow === countrySortingArrow) {
            areaSortingArrow.attributes.getNamedItem('arrow').value = 'up-down';
            areaSortingArrow.innerHTML = '&#8597;';
        } else {
            countrySortingArrow.attributes.getNamedItem('arrow').value = 'up-down';
            countrySortingArrow.innerHTML = '&#8597;';
        }
        renderResult();
    }
}

function changedRadioButton(event) {    
    if (event.target === radioInputByRegion) {
        selectListArray = externalService.getRegionsList();
    } else if (event.target === radioInputByLanguage) {
        selectListArray = externalService.getLanguagesList();
    }
    
    renderSelectList();           
}

function renderSelectList() {
    let selectList = document.querySelector('#item-list');
    if (selectList) {
        selectList.remove();
    }
    selectList = appendMyElement(divSelect, 'select', null,
     {class: 'pointer', name: 'item-list', id: 'item-list'});
        if (!radioInputByRegion.checked && !radioInputByLanguage.checked) {
        selectList.setAttribute('disabled', 'disabled');
    }        
    
    selectList.addEventListener('input', showResult, false);

    selectListArray.unshift('Select value');
    for (item of selectListArray) {
        appendMyElement(selectList, 'option', item, {class: 'pointer'});
    }
}

function activateSearch() {
    missingData.style.display = 'block';
    ulRadio.removeEventListener('change', activateSearch, false);
}

function showResult(event) {    
    let selection = event.currentTarget.value;

    if (selection === 'Select value') {
        table.style.display = 'none';
        missingData.style.display = 'block';
    } else {        
        if (radioInputByRegion.checked) {
            countryArrayToShow = externalService.getCountryListByRegion(selection);
        } else if (radioInputByLanguage.checked) {
            countryArrayToShow = externalService.getCountryListByLanguage(selection);
        }
        table.style.display = 'table';
        missingData.style.display = 'none';
        
        renderResult();        
    }    
}

function renderResult() {
    if (countrySortingArrow.attributes.getNamedItem('arrow').value === 'up') {
        countryArrayToShow.sort((a, b) => a.name.localeCompare(b.name));
    } else if (countrySortingArrow.attributes.getNamedItem('arrow').value === 'down') {
        countryArrayToShow.sort((a, b) => b.name.localeCompare(a.name));
    } else if (areaSortingArrow.attributes.getNamedItem('arrow').value === 'up') {
        countryArrayToShow.sort((a, b) => a.area - b.area);
    } else {
        countryArrayToShow.sort((a, b) => b.area - a.area);
    }

    Array.from( tbody.querySelectorAll('tr') )
         .forEach( (element) => element.remove() );

    for (country of countryArrayToShow) {
        let trBody = appendMyElement(tbody, 'tr', null, {class: 'table-row'});
        
        appendMyElement(trBody, 'td', country.name);
        appendMyElement(trBody, 'td', country.capital);
        appendMyElement(trBody, 'td', country.region);

        let languages = Object.values(country.languages).reduce( (languageList, currentLanguage) => {
            languageList += `, ${currentLanguage}`;
            return languageList;
        })
        appendMyElement(trBody, 'td', languages);
        appendMyElement(trBody, 'td', country.area);

        let img = createMyElement('img', null, {src: country.flagURL}, {alt: `Flag of ${country.name}`});        
        appendMyElement(trBody, 'td', img);    
    }    
}