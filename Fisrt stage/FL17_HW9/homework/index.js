/* START TASK 1: Your code goes here */
function randomInteger(maximum) {
    return Math.ceil(Math.random()*maximum);
}

let table = document.querySelector('.colored-table');

const tableDimension = 3;

let specCell = document.querySelector(`#td${randomInteger(tableDimension)}${randomInteger(tableDimension-1) + 1}`);
specCell.innerHTML = 'Special '+ specCell.innerHTML;
specCell.style.padding = '0.75em';

table.addEventListener('click', clickOnCell, false);

function clickOnCell(event) {
    setTimeout(() => {
        let cell = event.target;   
        let row = +cell.dataset.row;
        let col = +cell.dataset.col;     
        if (+cell.dataset.toggled) {
            cell.style.backgroundColor = cell.dataset.preColor;
            cell.dataset.toggled = '0';
        } else {
    
            if (col === 1) {
            
                for (let j = 1; j <= tableDimension; j++) {
                    let cellElement = document.querySelector(`#td${row}${j}`);
                    let cellColor = cellElement.style.backgroundColor +'';
                    let comparedColor = compareColors('blue', cellColor);
                    if (comparedColor[1]) {
                        cellElement.style.backgroundColor = comparedColor[0];
                        if (j === 1) {
                            cellElement.dataset.toggled = '1';
                            cellElement.dataset.preColor = cellColor;
                        }
                    }    
                }
        
            } else if (cell === specCell) {            
    
                for (let i = 1; i <= tableDimension; i++) {
                    for (let j = 1; j <= tableDimension; j++) {
                        let cellElement = document.querySelector(`#td${i}${j}`);
                        let cellColor = cellElement.style.backgroundColor + '';
                        let comparedColor = compareColors('green', cellColor); 
                        if (comparedColor[1] === true) {
                            cellElement.style.backgroundColor = comparedColor[0];
                            if (i === row && j === col) {
                                cellElement.dataset.toggled = '1';
                                cellElement.dataset.preColor = cellColor;
                            }
                        }    
                    }
                }
        
            } else {
                let cellColor = cell.style.backgroundColor + '';
                cell.style.backgroundColor = 'yellow';
                cell.dataset.toggled = '1';
                cell.dataset.preColor = cellColor;
            }
        }
    }, delay);
}

function compareColors (newcolor, currentColor) {
    const priority = ['white', 'green', 'blue', 'yellow'];
    if (priority.indexOf(newcolor) > priority.indexOf(currentColor)) {
        return [newcolor, true];
    } else {
        return [currentColor, false];
    }
}
/* END TASK 1 */

/* START TASK 2: Your code goes here */
let messageBoxContainer = document.querySelector('#message-box-container');
let messageBox = document.querySelector('#message-box');
let inputBox = document.querySelector('#input-box');
let inputButton = document.querySelector('#input-button');

const defaultValue = 'Type phone number in format: +380*********';
const errorMessage = 'Type number does not follow format: +380*********';
const sentMessage = 'Data was successfully sent';
const delay = 16.67;
inputBox.value = defaultValue;

inputBox.addEventListener('focus', gotFocusInputBox, false);
inputBox.addEventListener('blur', lostFocusInputBox, false);
inputBox.addEventListener('input', changedInput, false);
inputButton.addEventListener('click', sendNumber, false);

function gotFocusInputBox(event) {
    let inputElement = event.target;
    let inputValue = inputElement.value;
    inputElement.style.fontWeight = '600';
        if (inputValue === defaultValue) {
            setTimeout(() => {
            event.target.value = '';            
            inputElement.style.color = 'black';           
        }, delay);
    }
}

function lostFocusInputBox(event) {
    let inputElement = event.target;
    let inputValue = inputElement.value;
    inputElement.style.fontWeight = '500';
    if (inputValue === '') {
        setTimeout(() => {
            event.target.value = defaultValue;
            inputElement.style.color = 'grey';
            inputElement.style.borderColor = 'black';           
        }, delay);
    }
}

function changedInput(event) {   
    let inputElement = event.target;
    let inputValue = inputElement.value;
    
    if (/^\+380[\d]{9}$/.test(inputValue)) {
        setTimeout(() => {
            messageBox.innerHTML = '';
            messageBoxContainer.style.backgroundColor = 'white';
            inputElement.style.borderColor = 'black';
            messageBoxContainer.style.visibility = 'hidden';
            inputButton.disabled = false;
        }, delay);
    } else {
        setTimeout(() => {
            inputButton.disabled = true;
            messageBoxContainer.style.visibility = 'visible';
            messageBox.innerHTML = errorMessage;
            messageBoxContainer.style.backgroundColor = 'tomato'; 
            inputElement.style.borderColor = 'red';
        }, delay);
    }
}

function sendNumber() {
    setTimeout(() => {
        messageBox.innerHTML = sentMessage;
        messageBoxContainer.style.backgroundColor = 'green';
        messageBoxContainer.style.visibility = 'visible';
    }, delay)    
}
/* END TASK 2 */

/* START TASK 3: Your code goes here */
let ball = document.querySelector('.ball-image');
let court = document.querySelector('.court-area');
let aTeamHoop = document.querySelector('.team-a-hoop');
let bTeamHoop = document.querySelector('.team-b-hoop');
let currentX = 311;
let currentY = 145;
const ballSpeed = 0.5;
const squarePower = 2;
let animationTime;
const radialSpeed = 1;
let angle;
let rotateAngle = 0;

court.addEventListener('click', moveBall);
document.addEventListener('click', checkHoops);
document.addEventListener('score', scoreTheBall);

function moveBall(event) {
    let domRect = event.currentTarget.getBoundingClientRect();
    let futureX = event.clientX - domRect.x;
    let futureY = event.clientY - domRect.y;    

    let distance = Math.sqrt(Math.pow(currentX - futureX, squarePower) +
     Math.pow(currentY - futureY, squarePower));     
    animationTime = distance / ballSpeed;
    angle = distance*radialSpeed;
    rotateAngle+=angle;
    currentX = futureX;
    currentY = futureY;
    
    ball.style.left = futureX - '20' +'px';
    ball.style.top = futureY - '20' +'px';
    ball.style.transform = `rotate(${rotateAngle}deg)`;    
    ball.style.transitionProperty = 'top, left, transform';
    ball.style.transitionTimingFunction = 'ease-out';
    ball.style.transitionDuration = animationTime + 'ms';
    ball.style.transitionDelay = delay + 'ms';
    
    court.removeEventListener('click', moveBall);    
    setTimeout(() => {
        court.addEventListener('click', moveBall);      
    }, animationTime);      
}

function checkHoops(event) {
    setTimeout(() => {
        if (event.target === aTeamHoop || event.target === bTeamHoop) {        
            let scoreEvent = new Event('score', {bubbles: true});
            event.target.dispatchEvent(scoreEvent);            
        }
    }, delay + animationTime);    
}

function scoreTheBall(event) {
    let target = event.target;
    document.querySelector('.goal-message').remove();
    let task3 = document.querySelector('#task3');
    let scoreMessage = appendMyElement(task3, 'h3', null, {class: 'goal-message'});
    scoreMessage.append('Team ');
    let scoringTeam = appendMyElement(scoreMessage, 'span', null, {class: 'scoring-team'});
    scoreMessage.append(' scored!');
    scoreMessage.style.opacity = '1';    
    
    let aTeamScore = document.querySelector('.a-team-score');
    let bTeamScore = document.querySelector('.b-team-score');

    if (target.dataset.teamName === 'B') {
        scoreMessage.style.color = 'blue';
        scoringTeam.innerText = 'A';
        aTeamScore.innerText++; 
    } else {
        scoreMessage.style.color = 'red';
        scoringTeam.innerText = 'B';
        bTeamScore.innerText++; 
    }   
    
    scoreMessage.style.visibility = 'visible';
    scoreMessage.style.opacity = '0';

    function createMyElement(tag, content, attributes) {
        let element = document.createElement(tag);
        if (attributes) {
            for (const [key, value] of Object.entries(attributes)) {
                element.setAttribute(key, value);
            }
        }
        
        if (content || content === 0 || /^\s$/.test(content)){
            element.append(content);
        }        
        return element;
    }
    
    function appendMyElement(targetElement, tag, content, attributes) {
        let element = createMyElement(tag, content, attributes);
        targetElement.append(element);    
        return element;
    }
}
/* END TASK 3 */
