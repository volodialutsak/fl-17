const root = document.getElementById('root');
let textArea = document.querySelector('#modifyItemInput');
    textArea.rows = 7;
    textArea.cols = 20;
    textArea.maxLength = '140';
let formButtons = document.querySelector('.formButtons');
let addTweetButton = document.querySelector('.addTweet');
let listPage = document.querySelector('.listPage');
let modifyItem = document.querySelector('#modifyItem');
let modifyItemHeader = document.querySelector('#modifyItemHeader');
let addTweetHeader = createMyElement( 'h1', 'Add tweet');
let saveButton = document.querySelector('#saveModifiedItem');
let cancelButton = document.querySelector('#cancelModification');
    addTweetHeader.style.display = 'none';
    modifyItem.prepend(addTweetHeader);
let tweetItems = document.querySelector('#tweetItems');
let tweetList = document.querySelector('#list');
let navigationButtons = document.querySelector('#navigationButtons');
let messageBox = document.querySelector('#alertMessage');

let goToLikedButton = appendMyElement(navigationButtons, 'button', 'Go to liked',
 {class: 'go-to-liked-button button'});
let backButton = appendMyElement(navigationButtons, 'button', 'Back',
 {class: 'back-button buuton'});

const delay = 10;
const messageTime = 3000;

window.addEventListener('hashchange', renderPage);
window.addEventListener('load', renderPage )
addTweetButton.addEventListener('click', goToAddingPage);
saveButton.addEventListener('click', saveText);
cancelButton.addEventListener('click', cancel);
goToLikedButton.addEventListener('click', goToLikedPage);
backButton.addEventListener('click', goBack);

function goToAddingPage() {
    location.hash = '#/Add';
}

function goToEditingPage(e) {   
    location.hash = `#/edit/:${e.target.dataset.id}`;   
}

function removeTweet(e) {
    let id = e.target.dataset.id;
    localStorage.removeItem(id);
    showMessage(`You've just removed tweet with id ${id}!`);     
    renderPage();   
}

function likeTweet(e) {
    console.log('Like clicked!');
    let id = e.target.dataset.id;
    let tweetObject = JSON.parse( localStorage.getItem(id) );
    if (e.target.dataset.liked === 'true') {
        tweetObject.liked = false;
        e.target.innerText = 'Like';
        showMessage(`Sorry you no longer like tweet with id ${id}!`);         
    } else {
        tweetObject.liked = true;
        e.target.innerText = 'Unlike';
        showMessage(`Hooray! You liked tweet with id ${id}!`);
    }
    e.target.dataset.liked = tweetObject.liked;
    let parsedTweet = JSON.stringify(tweetObject);
    localStorage.setItem(id, parsedTweet);
    
    renderPage();
    }

function goToLikedPage() {
    location.hash = '#/liked';
}

function goBack() {
    history.back();
}

function renderPage() {    
    if (location.hash === '#/Add') {
        listPage.style.display = 'none';
        addTweetHeader.style.display = 'block';
        modifyItem.style.display = 'block';
        modifyItemHeader.style.display = 'none';
    } else if (location.hash === '#/liked') {
        if(!isThereLiked()) {
            history.back();
        }
        listPage.style.display = 'block';
        addTweetHeader.style.display = 'none';
        modifyItem.style.display = 'none';
        goToLikedButton.style.display = 'none';        
        backButton.style.display = 'inline';
        addTweetButton.style.display = 'none';  
        showtweets(true);
    } else if (location.hash === '') {
        if (isThereLiked()) {
            goToLikedButton.style.display = 'inline';
        } else {
            goToLikedButton.style.display = 'none'; 
        }
        addTweetButton.style.display = 'inline';
        backButton.style.display = 'none';
        listPage.style.display = 'block';
        addTweetHeader.style.display = 'none';
        modifyItem.style.display = 'none';
        showtweets(false);
    } else {
        listPage.style.display = 'none';
        addTweetHeader.style.display = 'block';
        modifyItem.style.display = 'block';
        modifyItemHeader.style.display = 'none';
        let id = location.hash.match(/[0-9]+/g);
        console.log(id);
        let tweetObject = JSON.parse( localStorage.getItem(id) );
        textArea.value = tweetObject.text;
        saveButton.dataset.id = id;
    }
}

function saveText() {    
    if(textArea.value.trim() !== ''){
        if (isTrehSuchTweet(textArea.value.trim())){
            showMessage('Error! You can\'t tweet about that');
        } else {
            if (location.hash === '#/Add') {
                let id = findNextId();
                console.log(id);
                let parsedTweet = JSON.stringify({id: id, text: textArea.value, liked: false});
               
                localStorage.setItem(id, parsedTweet);
                textArea.value = '';
                history.back();                
            } else {
                let id = saveButton.dataset.id;
                let tweetObject = JSON.parse( localStorage.getItem(id) );
                let liked = tweetObject.liked;
    
                let parsedTweet = JSON.stringify({id: id, text: textArea.value, liked: liked});
                localStorage.setItem(id, parsedTweet);
                textArea.value = '';
                history.back();                
            }
            messageBox.style.visibility = 'hidden';
        }        
    }    
}

function cancel() {
    textArea.value = '';
    history.back();  
    messageBox.style.visibility = 'hidden';
}

function showtweets(onlyliked) {
    setTimeout(() => {
        tweetItems.lastElementChild.remove();    
        let tweetList = appendMyElement(tweetItems, 'ul', null, {id: 'list'});
        let tweetArray = getTweetArray();
        
        if (onlyliked) {            
            tweetArray = tweetArray.filter((element) => element.liked);
        }
    
        tweetArray.forEach((element) => {
            let tweet = appendMyElement(tweetList, 'li', null,
            {class: 'tweet-list-item'})
           let tweetText = appendMyElement(tweet, 'p', element.text,
            {class: element.liked ? 'tweet-paragraph liked' : 'tweet-paragraph',
             'data-id': element.id, 'data-liked': element.liked});
            tweetText.addEventListener('click', goToEditingPage);
           let removeButton = appendMyElement(tweet, 'button', 'Remove',
            {class: 'remove-button button', 'data-id': element.id, 'data-liked': element.liked});
            removeButton.addEventListener('click', removeTweet);
           let likeButton = appendMyElement(tweet, 'button',
           element.liked ? 'Unlike': 'Like',
            {class: 'like-button button', 'data-id': element.id, 'data-liked': element.liked});
           likeButton.addEventListener('click', likeTweet);  
        })        
    }, delay);   
}

function getTweetArray() {
    let tweetArray = [];
    for (let i = 0; i < localStorage.length; i++) {
        let key = localStorage.key(i);        
        let tweetObject = JSON.parse( localStorage.getItem(key) );
        tweetArray.push(tweetObject);
    }        
        tweetArray.sort((a, b) => a.id - b.id);
        
        return tweetArray;
}

function findNextId() {
    let tweetArray = getTweetArray();
    let maxIdNumber = tweetArray.reduce((maxId, currentTweet) => {
        return +maxId > +currentTweet.id ? +maxId: +currentTweet.id;
    }, 0)
    return maxIdNumber + 1;
}

function isThereLiked() {
    let tweetArray = getTweetArray();
    return tweetArray.some( element => element.liked);    
}

function isTrehSuchTweet(tweet) {
    let tweetArray = getTweetArray();
    return tweetArray.some( element => element.text === tweet); 
}

function showMessage(message) {    
    messageBox.firstElementChild.innerText = message;
    messageBox.style.visibility = 'visible';
    setTimeout( () => {
        messageBox.style.visibility = 'hidden';
    }, messageTime);
}

function createMyElement(tag, content, attributes) {
    let element = document.createElement(tag);
    if (attributes) {
        for (const [key, value] of Object.entries(attributes)) {
            element.setAttribute(key, value);
        }
    }
    
    if (content || content === 0 || /^\s$/.test(content)){
        element.append(content);
    }        
    return element;
}

function appendMyElement(targetElement, tag, content, attributes) {
    let element = createMyElement(tag, content, attributes);
    targetElement.append(element);    
    return element;
}
