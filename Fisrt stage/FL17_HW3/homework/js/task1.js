let money, years, percent;
let moneyIsValidated = false;
let yearsIsValidated = false;
let percentIsValidated = false;


while (!moneyIsValidated) {
    money = +prompt('Please enter initial amount of money (number should be more than 1000):');
    moneyIsValidated = !isNaN(money) && money >= 1000;

    if (moneyIsValidated && money !== +money.toFixed(2) && !confirm(`Did you mean ${money.toFixed(2)} ?`)) {
        moneyIsValidated = false;
    }

    if (!moneyIsValidated) {
        alert('Invalid input data. Try again.')
    }
}
money = +money.toFixed(2);

while (!yearsIsValidated) {
    years = +prompt('Please enter years (number should be not less than 1):');
    yearsIsValidated = !isNaN(years) && years >= 1;

    if (yearsIsValidated && years !== Math.floor(years) && !confirm(`Did you mean ${Math.floor(years)} years?`)) {
        yearsIsValidated = false;
    }

    if (!yearsIsValidated) {
        alert('Invalid input data. Try again.')
    }
}
years = Math.floor(years);

while (!percentIsValidated) {
    percent = +prompt('Please enter percent rate a year (number should be in range from 0 to 100):');
    percentIsValidated = !isNaN(percent) && percent > 0 && percent <= 100;

    if (percentIsValidated && percent !== +percent.toFixed(4) && !confirm(`Did you mean ${percent.toFixed(4)}% ?`)) {
        percentIsValidated = false;
    }

    if (!percentIsValidated) {
        alert('Invalid input data. Try again.')
    }
}
percent = +percent.toFixed(4);

let total = Math.pow(1 + percent/100, years) * money;
let profit = total - money;
total = total.toFixed(2);
profit = profit.toFixed(2);

alert(`Initial amount: ${money}
Number of years: ${years}
Percentage of year: ${percent}\n
Total profit: ${profit}
Total amount: ${total}
`);
