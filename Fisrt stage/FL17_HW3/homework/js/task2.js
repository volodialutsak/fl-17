let range = 8;
let randomNumber;
let userAttempts = 3;
let userWantsToPlay = true;
let userMoney = 0;
let currentPrize;
let firstPrize = 100;
let secondPrize = 50;
let thirdPrize = 25;

function randomGenerator(biggestNumber) {
    return Math.floor(Math.random() * (biggestNumber + 1));
}

const varificateNumber = () => {
    function incorrectInputAlert() {
        alert(`Incorrect input. You should enter an integer between 0 and ${range}!`);
        number = varificateNumber();
        return number;
    }

    let number = prompt(`
    Choose a roulette pocket number from 0 to ${range}
    Attempts left: ${userAttempts}
    Total prize: ${userMoney}$
    Possible prize on current attempt: ${currentPrize}$
    `);
  
    if (number === null) {
        return number;
    } else {
        if (number === '' || number !== number.trim()) {
            number = incorrectInputAlert();
        } else {
            number = +number;
        }
    
        if (number === null) {
            return number;
        } else {
            let numberIsValidated = !isNaN(number) && number >= 0 &&
            number <= range && number === Math.floor(number);
       
           if (numberIsValidated) {
               return number;
           } else {
               return incorrectInputAlert();
           }
        }
        
    }
    
}

let toPlayQuestion = confirm('Do you want to play a game?');
if (!toPlayQuestion) {
    alert('You did not become a billionaire, but can.');
} else {
    randomNumber = randomGenerator(range);
    while (userWantsToPlay) {

        if (userAttempts === 3) {
            currentPrize = firstPrize;
        } else if (userAttempts === 2) {
            currentPrize = secondPrize;
        } else {
            currentPrize = thirdPrize;
        }

        let guess = varificateNumber();

        if(guess === null) {
            userWantsToPlay = false;
        } else {
            if (guess !== randomNumber) {
                userAttempts--;
            } else {
                userMoney += currentPrize;
                if ( confirm(`Congratulation, you won! Your prize is: ${userMoney}$. Do you want to continue?`) ) {
                    userAttempts = 3;
                    firstPrize *= 2;
                    secondPrize *= 2;
                    thirdPrize *= 2;
                    range += 4;
                    randomNumber = randomGenerator(range);
                } else {
                    userWantsToPlay = false;
                }
            }
        }
       

        if (!userAttempts || !userWantsToPlay) {
            alert(`Thank you for your participation. Your prize is: ${userMoney}$`);
            if (confirm('Do you want to play again?')) {
                userAttempts = 3;
                firstPrize = 100;
                secondPrize = 50;
                thirdPrize = 25;
                range = 8;
                userWantsToPlay = true;
                randomNumber = randomGenerator(range);
            } else {
                userWantsToPlay = false;
            } 
        }
    }
}