function getAge(birthday) {
    let now = new Date();
    let age = now.getFullYear() - birthday.getFullYear();
 
    if (now.getMonth() < birthday.getMonth()) {
        return --age;
    } else if (now.getMonth() === birthday.getMonth() &&
                now.getDate() < birthday.getDate()) {
        return --age;
    }
    return age;
}

function getWeekDay(date) {
    date = new Date(date);
    const weekDayArray = ['Sunday','Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']

    return weekDayArray[date.getDay()];
}

const getAmountDaysToNewYear = () => {
    const msInDay = 86400000;
    let now = new Date();
    let nowDate = new Date(now.getFullYear(), now.getMonth(), now.getDate());
    let newYearDate = new Date(now.getFullYear() + 1, 0, 1);

    return Math.round( (newYearDate - nowDate)/msInDay );
}
 
 function getProgrammersDay(year) {
    const numberOfProgDay = 256;
    let programmersDay = new Date(year, 0, numberOfProgDay);
    return `${programmersDay.getDate()} ${programmersDay.toLocaleString('en', {month: 'short'})}, `+
    `${year} (${getWeekDay(programmersDay)})`;
 }

function howFarIs(day) {
    let now = new Date();
    let today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
    let nextDay;
    let leftDays = -1;
    let numberOfDaysIsFound = false;

    while(!numberOfDaysIsFound) {
        leftDays++;
        nextDay = new Date(today.getFullYear(), today.getMonth(), today.getDate() + leftDays);

        if (day.toLowerCase() === getWeekDay(nextDay).toLowerCase()) {
            numberOfDaysIsFound = true;
        }
    }

        if(!leftDays) {
            return `Hey, today is ${getWeekDay(today)} =)`;
        } else {
            return `It's ${leftDays} day(s) left till ${getWeekDay(nextDay)}.`
        }
}

function isValidIdentifier(varString) {
    let reg = RegExp('^[\\p{Alpha}_$][\\p{Alpha}_$\\d]*$', 'u');
    return reg.test(varString);
}

function capitalize(text) {
    return text.replace(/(\b\w)/g, str => str.toUpperCase());
}

function isValidAudioFile(fileName) {
    return /^[a-z]+\.(mp3|flac|alac|aac)$/i.test(fileName);
}

function getHexadecimalColors(text) {
    return text.match(/#(\b([0-9a-f]{3}|[0-9a-f]{6}))\b/gi) || [];
}

function isValidPassword(password) {
    return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[\S\s]{8,}$/.test(password);
}

function addThousandsSeparators(number) {
    return String(number).replace(/\d/g, (digit, offSet, input) => {
        const digitsForSeparator = 3;
        if ( !((input.length - offSet) % digitsForSeparator) && offSet) {
            return `,${digit}`;
        }
        return digit;
    });
}

function getAllUrlsFromText(text) {
    return text.match(/https?:\/\/([\w~-]+\.)+\w+\/?/gi) || [];
}