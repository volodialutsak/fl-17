function reverseNumber(num) {
    let reversedNumber = '';
    let numString = num + '';
    toCheckForZero = true;

    if (numString[0] === '-') {
        reversedNumber += '-';
    }
    
    for (let i = numString.length - 1; i >= 0; i--) {
        if (toCheckForZero) {
            if (numString[i] !== '0') {
                reversedNumber += numString[i];
                toCheckForZero = false;
            }
        } else {
            if (numString[i] !== '-') {
                reversedNumber += numString[i];
            }
        }
    }

    return +reversedNumber;
}

function forEach(arr, func) {
    for (let item of arr) {
        func(item);
    }    
}

function map(arr, func) {
    let mappedArr = [];
    forEach(arr, (item) => {
        mappedArr.push(func(item));
    })

    return mappedArr;
}

function filter(arr, func) {
    let filteredArr = [];  
    forEach(arr, (item) => {
        if (func(item)) {
            filteredArr.push(item); 
        }
    })

    return filteredArr;
}

function getAdultAppleLovers(data) {
    const ageLimit = 18;
    
    let appleLoversData = filter(data, (item) => {
        return item['favoriteFruit'] === 'apple' && item['age'] > ageLimit;
    });

    let appleLoversNames = map(appleLoversData, (item) => {
        return item['name']
    });

    return appleLoversNames;
}

function getKeys(obj) {
    let keyArr = []
    for (let prop in obj) {
        keyArr.push(prop);
    }
    return keyArr;
}

function getValues(obj) {
    let valueArr = map(getKeys(obj), (item) => {
        return obj[item];
    })

    return valueArr;
}

function showFormattedDate(dateObj) {
    return `It is ${dateObj.getDate()}`+
    ` of ${dateObj.toLocaleString('en-US', {month: 'short'})}`+
    `, ${dateObj.getFullYear()}`;
}