function isEquals(a, b) {
    return !(a-b);
}

// isBigger() figures out if 1st argument is bigger than 2nd without using if...else clause,
//comperison operators and Math library.
//max(a, b) = ( (a+b) + |(a-b)| ) / 2,
// but without Math.abs() it cannot be implemented
function isBigger(a, b) {
    const thirtyOneBit = 31;

    //Let's build our own abs method - returns absolute value
    function abs(number) {
        let mask = number>>thirtyOneBit;
        number ^= mask;
        number -= mask;
        return number
    }

    //calculates the biggest of two numbers
    const twoDivider = 2;
    let maxNumber = ( a + b + abs(a - b) ) / twoDivider;

    //returns 'true' only if 1st parameter is bigger
    return !isEquals(a, b) && isEquals(a, maxNumber);
}

function storeNames(...args) {    
    return args;    
}

function getDifference(a, b) {
    return a > b ? a - b : b - a;
}

function negativeCount(arr) {
    const counter = (count, value) => {
        if (value < 0) {
            count++;
        } 
        return count;
    }

    return arr.reduce(counter, 0)
}

function letterCount(stringToCount, stringToFound) {
    let primeLetterArray = stringToCount.split('');
   
    let allCombinationsArray = primeLetterArray.map((char, index) => {        
        return primeLetterArray.slice(index, index + stringToFound.length).join('');
    })
      
    return allCombinationsArray.reduce((count, currentItem) => {
        return currentItem === stringToFound ? ++count : count;
    }, 0)
}

function countPoints(arr) {
    const winPoints = 3;
    const drawPoints = 1;

    const modifiedArr = arr.map((elem) => {
        return elem.split(':');
    });
    
    const counter = (count, value) => {
        if (+value[0] > +value[1]) {
            count += winPoints;
        } else if (+value[0] === +value[1]) {
            count += drawPoints;
        }

        return count;
    }

    return modifiedArr.reduce(counter, 0);
}