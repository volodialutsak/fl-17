const firstNumber = {value: '0'};
const secondNumber = {value: ''};
let operation = '';
let designator = firstNumber;
let error = false;

$(function() {
    renderScreen();    
    $('.key-button').on('click', clickButton);

    function renderScreen() {
        $('.screen-block').text(getScreenValue());
    }

    function getScreenValue() {
        return firstNumber.value + operation + secondNumber.value;
    }

    function renderError() {
        $('.screen-block').text('ERROR');
        $('.screen-block').addClass('red-text');
    }

    function restoreDefault() {
        firstNumber.value = '0';
        secondNumber.value = '';
        operation = '';
        designator = firstNumber;
        error = false;
        $('.screen-block').removeClass('red-text');
    }

    function clickButton(e) {
        let key = e.target.innerText;
        let keyType = e.target.dataset.keyType;
        let result;      
        
        switch (keyType) {
            case 'digit':
                if (designator.value === '0') {
                    designator.value = key;

                    if (error === true) {
                        error = false;
                        $('.screen-block').removeClass('red-text');
                    }                    
                } else {
                    designator.value += key;
                }
                renderScreen();
                break;
            case 'operator':
                if (!error) {
                    if (getScreenValue() !== $('.screen-block').text()) {
                        firstNumber.value = $('.screen-block').text()
                    }
                    operation = key;
                    designator = secondNumber;
                    renderScreen();
                }                
                break;
            case 'cancel':
                restoreDefault();
                renderScreen();
                break;
            case 'calculate':                
                if (secondNumber.value) {
                    switch (operation) {
                        case '/':
                            if(secondNumber.value === '0') {
                                result = false;                                
                            } else {
                                result = +firstNumber.value / +secondNumber.value;
                            }                            
                            break;
                        case '*':
                            result = +firstNumber.value * +secondNumber.value;
                            break;
                        case '+':
                            result = +firstNumber.value + +secondNumber.value;
                            break;
                        case '-':
                            result = +firstNumber.value - +secondNumber.value;
                            break;
                        default: console.log('Error accured!');
                    }

                    let screenValue = getScreenValue();                    
                    restoreDefault();

                    if (result === false) {
                        error = true;
                        renderError();
                    } else {
                        $('.screen-block').text(result.toString());
                        addLogRow(screenValue, result.toString());
                    }
                }
                break;
            default: console.log('Error accured!');
        }  
    }

    function addLogRow(screenValue, result) {
        $('.logs-block').scrollTop(0);
        let index = $('.log-row').length;
        
        $('.logs-block').prepend(`<div class="log-row" id="log${index}">
            <div class="circle" data-id="circle"></div>
            <div class="equation" data-id="equation">${screenValue}=${result}</div>
            <div class="close-icon" data-id="remove">&#10006;</div>
            </div>`);

            if(screenValue.includes('48') || result.includes('48')) {
                $(`#log${index}`).find('[data-id="equation"]').css('text-decoration', 'underline');
            }
            
            $(`#log${index}`).on('click', logRowClickHandler);
    }

    function logRowClickHandler(e) {
        console.log(e.target);
        if ($(e.target).data('id') === 'circle') {
            $(e.target).toggleClass('red-background');  
        } else if ($(e.target).data('id') === 'remove') {
            $(e.target).parent().remove();
        }
    }

    $('.logs-block').on('scroll', (e) => {
        console.log('Scroll Top: ' + e.target.scrollTop);
    })

})
