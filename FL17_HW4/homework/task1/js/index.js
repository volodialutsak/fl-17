function createMyElement(tag, content, attributes) {
    let element = document.createElement(tag);
    if (attributes) {
        for (const [key, value] of Object.entries(attributes)) {
            element.setAttribute(key, value);
        }
    }
    
    if (content || content === 0 || /^\s$/.test(content)){
        element.append(content);
    }        
    return element;
}

function appendMyElement(targetElement, tag, content, attributes) {
    let element = createMyElement(tag, content, attributes);
    targetElement.append(element);    
    return element;
}

let usersArr = [];
const delay = 17;
let editForm = document.querySelector('.edit-form');
let spinner = document.querySelector('.spinner');

function getUsers() {
    startSpinner();
    fetch('https://jsonplaceholder.typicode.com/users')
        .then((response) => response.json())
        .then(users => { 
            console.log(users);     
            usersArr = users;
            renderUsers(usersArr);
            stopSpinner();
        })
        .catch(error => {
            console.log(error);
            stopSpinner();
        }) 
}

let root = document.querySelector('.root');

function renderUsers(users) {
    root.innerHTML = null;
   
    users.forEach(user => {        
        let userBlock = appendMyElement(root, 'div', null,
         {'data-id': `${user.id}`, class: 'user-block'});
        appendMyElement(userBlock, 'p', `${user.id}`, {class: 'user-id'});
        let userName = appendMyElement( userBlock, 'p', `${user.name}`,
         {'data-id': `${user.id}`, class: 'user-name'});
        let deleteButton = appendMyElement(userBlock, 'button', `Delete`,
         {'data-id': `${user.id}`, class: 'delete-button'});
        
        deleteButton.addEventListener('click', deleteUser);
        userName.addEventListener('click', editUser);
    });
}

function deleteUser(e) {
    let userId = +e.target.dataset.id;
    startSpinner();
    fetch(`https://jsonplaceholder.typicode.com/users/${userId}`, {method: 'DELETE'})
    .then((response) => {        
        if (response.status === +'200'){
            deleteUserFromArray(userId);
            renderUsers(usersArr);
        }
        stopSpinner();
    })    
    .catch(error => {
        console.log(error);
        stopSpinner();
    });
}

function deleteUserFromArray(id) {
    usersArr = usersArr.filter(user => user['id'] !== id)
}

function editUser(e) {
    let userId = +e.target.dataset.id;
    let index = usersArr.findIndex(element => element.id === userId);
    renderEditForm(editForm, usersArr[index]);

    setTimeout(() => {
        root.style.display = 'none';
    }, 0);

    setTimeout(() => {
        editForm.style.display = 'flex';
        editForm.style.flexDirection = 'column';
        document.querySelector('input[id=\'id\']').disabled = true;
    }, delay);
}

function renderEditForm(editForm, userData) {
    editForm.innerHTML = null;
    let saveButton = appendMyElement(editForm, 'button', `Save`, {class: 'save-button button'});
    let cancelButton = appendMyElement(editForm, 'button', `Cancel`, {class: 'cancel-button button'});
    saveButton.addEventListener('click', saveChanges);
    cancelButton.addEventListener('click', closeEdit);

    renderObject(editForm, userData);

    function renderObject(objContainer, data) {
        for(const [key, value] of Object.entries(data)) {
            if (typeof value === 'object') {
                let fieldSet = appendMyElement(objContainer, 'fieldset', null, {'data-key': key});
                appendMyElement(fieldSet, 'legend', key, {class: 'field-set'});
                renderObject(fieldSet, value);
            } else {
                let inputContainer = appendMyElement(objContainer, 'div', null,
                {class: 'input-container'});
                appendMyElement(inputContainer, 'label', `${key}`,
                {'data-key': `${key}`, 'for': `${key}`, class: 'label'})
                appendMyElement(inputContainer, 'input', null,
                {'data-parent': objContainer.dataset.key, 'data-key': `${key}`,
                 'id': `${key}`, 'value': value, 'type': 'text', class: 'input'})
            }            
        }    
    }    
}

function closeEdit() {
    renderUsers(usersArr);
    setTimeout(() => {
        root.style.display = 'flex';
    }, delay);

    setTimeout(() => {
        editForm.style.display = 'none';        
    }, 0);
}

function saveChanges() {
    let userId = +editForm.querySelector('input[id = \'id\']').value;
    let index = usersArr.findIndex(element => element.id === userId);
    let userData = JSON.parse(JSON.stringify(usersArr[index]));    
    userData = parseDataFromForm(userData);
    
    startSpinner();
    fetch(`https://jsonplaceholder.typicode.com/users/${userId}`, {
        method: 'PUT',
        body: JSON.stringify(userData),
        headers: {
          'Content-type': 'application/json; charset=UTF-8'
        }
      })
        .then(response => {
            if (response.status === +'200'){
                usersArr[index] = userData;                
                closeEdit();
                stopSpinner();
            }  
        })
        .catch(error => {
            console.log(error);
            stopSpinner();
        });
      /*  */
}

function parseDataFromForm(obj) {
    let parent;    
    parseData(obj);
    function parseData(obj1) {
        for (let key in obj1) {            
            if (Object.prototype.hasOwnProperty.call(obj1, key)) {                
                if (typeof obj1[key] === 'object') {
                    parent = key;
                    parseData(obj1[key]);
                    parent = undefined;
                } else {
                    let fields = editForm.querySelectorAll(`input[id = '${key}']`);
                    for (let field of fields) {                        
                        if (field && field.dataset.parent === parent + '') {
                            obj1[key] = field.value;
                            if (key === 'id') {
                                obj1[key] = +obj1[key];
                            }                
                        }   
                    }                                 
                }
            }
        }
    }    
    return obj;
}

function startSpinner() {
    document.body.style.pointerEvents = 'none';
    spinner.style.display = 'block';
}

function stopSpinner() {
    document.body.style.pointerEvents = 'initial';
    spinner.style.display = 'none';
}

getUsers();