let userName = '';
const enterKeyCode = 13;

let webSocket = new WebSocket('ws://localhost:8080', 'json');

let input = document.querySelector('.input-message');
let sendButton = document.querySelector('.send-button');
sendButton.addEventListener('click', sendMessage);
input.addEventListener('keyup', KeyUp);

let messageSpace = document.querySelector('.message-space');

webSocket.onopen = function(event) {
    console.log('WebSocket has been opened', event);
    userName = prompt('Please enter your name.');
    input.disabled = false;
    sendButton.disabled = false;
}

function sendMessage() {
    let text = input.value;
    let date = new Date();
    let message = {
        text: text,
        time: getTime(date),
        user: userName
    }
    webSocket.send(JSON.stringify(message));
    console.log('Message is sent.');
    let myMessage = appendMyElement(messageSpace, 'div', null,
     {class: 'my-message-container message-container'});
    appendMyElement(myMessage, 'h6', message.user, {class: 'user-name'});
    appendMyElement(myMessage, 'p', message.text, {class: 'user-message my-message'});
    appendMyElement(myMessage, 'h6', message.time, {class: 'post-time'});
    myMessage.scrollIntoView();
    input.value = '';
}

function KeyUp(e) {
    if (e.keyCode === enterKeyCode) {
        e.preventDefault();
        sendMessage();
    }
}

webSocket.onmessage = function(event) {
    let message = JSON.parse(event.data);
    console.log('Message is received.');
    
    let othersMessage = appendMyElement(messageSpace, 'div', null,
     {class: 'others-message-container message-container'});
    appendMyElement(othersMessage, 'h6', message.user, {class: 'user-name'});
    appendMyElement(othersMessage, 'p', message.text, {class: 'user-message others-message'});
    appendMyElement(othersMessage, 'h6', message.time, {class: 'post-time'});
    othersMessage.scrollIntoView();
}
    
function getTime(dateObj) {
    return dateObj.toLocaleTimeString('en-US');
}

function createMyElement(tag, content, attributes) {
    let element = document.createElement(tag);
    if (attributes) {
        for (const [key, value] of Object.entries(attributes)) {
            element.setAttribute(key, value);
        }
    }
    
    if (content || content === 0 || /^\s$/.test(content)){
        element.append(content);
    }        
    return element;
}

function appendMyElement(targetElement, tag, content, attributes) {
    let element = createMyElement(tag, content, attributes);
    targetElement.append(element);    
    return element;
}