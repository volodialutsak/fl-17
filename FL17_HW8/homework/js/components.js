import scissors from '../img/scissors.png';
import paper from '../img/paper.png';
import rock from '../img/rock.png';
import {createMyElement} from './append-my-element';


function components() {
    
    let scissorsImg = createMyElement('img', null, {src: scissors, alt: 'scissors'});
    let paperImg = createMyElement('img', null, {src: paper, alt: 'paper'});
    let rockImg = createMyElement('img', null, {src: rock, alt: 'rock'});
    
    return {
        scissors: scissorsImg,
        paper: paperImg,
        rock: rockImg
        };    
}

export {components};