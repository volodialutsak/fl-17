function createMyElement(tag, content, attributes) {
    let element = document.createElement(tag);
    if (attributes) {
        for (const [key, value] of Object.entries(attributes)) {
            element.setAttribute(key, value);
        }
    }
    
    if (content || content === 0 || /^\s$/.test(content)){
        element.append(content);
    }        
    return element;
}

function appendMyElement(targetElement, tag, content, attributes) {
    let element = createMyElement(tag, content, attributes);
    targetElement.append(element);    
    return element;
}

export {createMyElement, appendMyElement};