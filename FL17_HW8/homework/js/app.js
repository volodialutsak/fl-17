import '../scss/styles.scss';
import { appendMyElement } from './append-my-element';
import {components} from './components.js';

const range = 3;

const buttonsArray = [];
const gameStats = {
    round: 1,
    playerScore: 0,
    computerScore: 0,
    playersResult: ''
};
let buttons = document.querySelector('.buttons');
let rock = appendMyElement(buttons, 'button', components().rock, {class: 'button', id: 'rock', 'data-id': 'Rock'});
let scissors = appendMyElement(buttons, 'button', components().scissors,
 {class: 'button', id: 'scissors', 'data-id': 'Scissors'});
let paper = appendMyElement(buttons, 'button', components().paper, {class: 'button', id: 'paper', 'data-id': 'Paper'});
buttonsArray.push(rock, scissors, paper);
console.log(rock.id);
buttonsArray.forEach(item => {
    item.addEventListener('click', checkResult);
})

document.querySelector('.reset-link').addEventListener('click', resetGame);

function checkResult(e) {
    let computerGuess = buttonsArray[generateNumber(range)];
    let playerGuess = e.currentTarget;
    if (playerGuess === rock && computerGuess === paper) {
        gameStats.computerScore++;
        gameStats.playersResult = 'LOST';
    } else if (playerGuess === rock && computerGuess === scissors) {
        gameStats.playerScore++;
        gameStats.playersResult = 'WON';
    } else if (playerGuess === paper && computerGuess === rock) {
        gameStats.playerScore++;
        gameStats.playersResult = 'WON';
    } else if (playerGuess === paper && computerGuess === scissors) {
        gameStats.computerScore++;
        gameStats.playersResult = 'LOST';
    } else if (playerGuess === scissors && computerGuess === rock) {
        gameStats.computerScore++;
        gameStats.playersResult = 'LOST';
    } else if (playerGuess === scissors && computerGuess === paper) {
        gameStats.playerScore++;
        gameStats.playersResult = 'WON';
    }
    
    if (gameStats.playerScore === range || gameStats.computerScore === range) {
        showFinalResult();
    } else {
        addToLogRound(playerGuess, computerGuess);
    }
    gameStats.round++;
}

function generateNumber(range) {
    return Math.floor(Math.random() * range);
}

function resetGame() {
    buttons.scrollIntoView();
    gameStats.round = 1;
    gameStats.playerScore = 0;
    gameStats.computerScore = 0;
    gameStats.playersResult = '';
    setTimeout(() => {
        if (document.querySelector('.log')) {
            document.querySelector('.log').remove();
        }
        if (document.querySelector('.final-result')) {
            document.querySelector('.final-result').remove();
        }    
    }, '170');
    
    switchButtons(true);
}

function addToLogRound(playerGuess, computerGuess) {
    if (!document.querySelector('.log')) {
        appendMyElement(document.body, 'div', null, {class: 'log'})
    }

    let log = document.querySelector('.log');
    let endOfMessage = `You've ${gameStats.playersResult}!`;
    if (playerGuess === computerGuess) {
        endOfMessage = 'It is a DRAW!';
    }
    let logMessage = `Round ${gameStats.round}, ${playerGuess.dataset.id} vs. ${computerGuess.dataset.id}, ` +
        endOfMessage;

    appendMyElement(log, 'p', logMessage, {class: 'log-message'});
    log.scrollIntoView();
}

function showFinalResult() {
    switchButtons(false);
    let finalResult = appendMyElement(document.body, 'div', null, {class: 'final-result'});
    let message = '';
    if (gameStats.playerScore > gameStats.computerScore) {
        message = 'You WON!!!';
    } else {
        message = 'You LOST!!!';
    }
    appendMyElement(finalResult, 'h3', message, {class: 'final-result-message'});
    finalResult.scrollIntoView();

}

function switchButtons(state) {
    buttonsArray.forEach(item => {
        item.disabled = !state;
    })
}

