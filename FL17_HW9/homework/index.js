class MagazineState {
    constructor(name, nextStatus) {
        this.name = name;
        this.NextStatus = nextStatus;
    }

    next() {
        return new this.NextStatus();
    }
}

class ReadyForPushNotification extends MagazineState {
    constructor() {
        super('readyForPushNotification', ReadyForApprove);
    }
}

class ReadyForApprove extends MagazineState {
    constructor() {
        super('readyForApprove', ReadyForPublish);
    }
}

class ReadyForPublish extends MagazineState {
    constructor() {
        super('readyForPublish', PublishInProgress)
    }
}

class PublishInProgress extends MagazineState {
    constructor() {
        super('publishInProgress', ReadyForPushNotification)
    }
}

class Magazine {
    constructor() {
        this.state = new ReadyForPushNotification();
        this.staff = [];
        this.articles = [];
        this.publishedArticles = [];
        this.followers = [];
    }

    nextState() {
        this.state = this.state.next();
        console.log(`Magazine state has been changed to ${this.state.name}`);
    }

    addEmployee(employee) {
        this.staff.push(employee);
        console.log(`New employee was added: ${employee.name} - ${employee.position}`);
    }

    subscribeFollower(follower, topic) {
        const emptyIndex = -1;
        let index = this.followers.findIndex(item => item.follower === follower);
        
        if (index === emptyIndex) {
            this.followers.push({follower: follower, topics: new Set().add(topic)});
            console.log(`${follower.name} subscription for topic "${topic}" was added`);
        } else {
            if(!this.followers[index].topics.has(topic)) {
                this.followers[index].topics.add(topic);
                console.log(`${follower.name} subscription for topic "${topic}" was added`);
            }            
        }
    }

    unsubscribeFollower(follower, topic) {
        const emptyIndex = -1;
        let index = this.followers.findIndex(item => item.follower === follower);

        if (index !== emptyIndex) {
            let result = this.followers[index].topics.delete(topic);
            if (result) {
                console.log(`${follower.name} subscription for topic "${topic}" was canceled`);
            }
        }
    }

    publishArticles() {
        this.publishedArticles = this.publishedArticles.concat(this.articles);        
        this.followers.forEach(follower => {            
            let topics = follower.topics.values();
            for (let topic of topics) {
                this.articles
                .filter(elem => elem.topic === topic)
                .forEach(elem => {
                    follower.follower.onUpdate(elem.article);
                })
            }            
        })
        this.articles = [];
    }    
}

class MagazineEmployee {
    constructor(name, position, company) {
        this.name = name;
        this.position = position;
        this.company = company;
        company.addEmployee(this);        
    }

    addArticle(article) {
        if (this.position !== 'manager' && (this.company.state instanceof ReadyForPushNotification ||
            this.company.state instanceof ReadyForApprove) ) {
                this.company.articles.push({topic: this.position, article: article});
                console.log(`${this.name} added article with "${this.position}" topic`);
                const articlesQuantity = 5;
                if (this.company.articles.length === articlesQuantity) {
                    this.company.nextState();
                }        
        }        
    }

    approve() {
        if (this.position !== 'manager') {
            console.log(`${this.name}, you do not have permissions to do it.`);
        } else if (this.company.state instanceof ReadyForPushNotification) {
            console.log(`Hello ${this.name}. You can't approve. We don't have enough of publications.`);
        } else if (this.company.state instanceof ReadyForApprove) {           
            console.log(`Hello ${this.name}. You've approved the changes.`);
            this.company.nextState();
        } else if (this.company.state instanceof ReadyForPublish) {
            console.log(`Hello ${this.name}. Publications have been already approved by you.`);
        } else if (this.company.state instanceof PublishInProgress) {
            console.log(`Hello ${this.name}. While we are publishing we can't do any actions.`);
        }
    }

    publish() {
        if (this.company.state instanceof ReadyForPushNotification) {
            console.log(`Hello ${this.name}. You can't publish. We are creating publications now.`);
        } else if (this.company.state instanceof ReadyForApprove) {            
            console.log(`Hello ${this.name}. You can't publish. We don't have a manager's approval.`);
        } else if (this.company.state instanceof ReadyForPublish) {
            console.log(`Hello ${this.name}. You've recently published publications.`);
            this.company.nextState();
            const delay = 600;
            setTimeout(() => {
                this.company.nextState();
            }, delay);            
            this.company.publishArticles();            
        } else if (this.company.state instanceof PublishInProgress) {
            console.log(`Hello ${this.name}. While we are publishing we can't do any actions.`);
        }
    }

} 

class Follower {
    constructor(name) {
        this.name = name;
        console.log('New follower was created: ' + this.name);
    }

    subscribeTo(magazine, topic) {
        magazine.subscribeFollower(this, topic);
    }

    unsubscribeFrom(magazine, topic) {
        magazine.unsubscribeFollower(this, topic);
    }

    onUpdate(data) {
        console.log(data, this.name);
    }
}


const magazine = new Magazine();
const manager = new MagazineEmployee('Andrii', 'manager', magazine);
const sport = new MagazineEmployee('Serhii', 'sport', magazine);
const politics = new MagazineEmployee('Volodymyr', 'politics', magazine);
const general = new MagazineEmployee('Olha', 'general', magazine);

const iryna = new Follower('Iryna');
const maksym = new Follower('Maksym');
const mariya = new Follower('Mariya');

//console.log(magazine.staff);

iryna.subscribeTo(magazine, 'sport');
maksym.subscribeTo(magazine, 'politics');
mariya.subscribeTo(magazine, 'politics');
mariya.subscribeTo(magazine, 'general');


sport.addArticle('something about sport');
politics.addArticle('something about politics');
general.addArticle('some general information');
politics.addArticle('something about politics again');

mariya.unsubscribeFrom(magazine, 'politics');
//mariya.unsubscribeFrom(magazine, 'general');

sport.approve() //you do not have permissions to do it
manager.approve();//Hello Andrii. You can't approve. We don't have enough of publications
politics.publish(); //Hello Volodymyr. You can't publish. We are creating publications now.
sport.addArticle('news about sport'); 
manager.approve(); //Hello Andrii. You've approved the changes
sport.publish(); //Hello Serhii. You've recently published publications.

/*
something about sport Iryna
news about sport Iryna
something about politics Maksym
something about politics Mariya
something about politics again Maksym
something about politics again Mariya
some general information Mariya
*/

manager.approve('news about sport'); //Hello Andrii. While we are publishing we can't do any actions