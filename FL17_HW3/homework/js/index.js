'use strict';

/**
 * Class
 * @constructor
 * @param size - size of pizza
 * @param type - type of pizza
 * @throws {PizzaException} - in case of improper use
 */ 

function Pizza(size, type) {
    if (type === undefined || size === undefined) {
        throw new PizzaException(`Required two arguments, given: ${arguments.length}`);
    } else if (!Pizza.allowedSizes.includes(size) || !Pizza.allowedTypes.includes(type)) {
        throw new PizzaException(`Invalid type`);
    } else {
        this.size = size;
        this.type = type;
    }

    this.extraIngredients = [];

    this.addExtraIngredient = (extraIngridient) => {
        if (!Pizza.allowedExtraIngredients.includes(extraIngridient)) {
            throw new PizzaException(`Invalid ingredient '${extraIngridient}'`);
        } else if (this.extraIngredients.includes(extraIngridient)) {
            throw new PizzaException(`Duplicate ingredient '${extraIngridient}'`);
        }
        this.extraIngredients.push(extraIngridient);
    }

    this.getPrice = function() {
        let sum = 0;
        sum += Pizza.prices[this.size];
        sum += Pizza.prices[this.type];
        sum += this.extraIngredients.reduce( (sumExtra, currentIngredient) => {
            return sumExtra + Pizza.prices[currentIngredient];
        }, 0)
        return sum;
    }

    this.getSize = () => {
        return this.size;
    }

    this.removeExtraIngredient = (extraIngridient) => {
        if (!Pizza.allowedExtraIngredients.includes(extraIngridient)) {
            throw new PizzaException(`Invalid ingredient '${extraIngridient}'`);
        } else if (!this.extraIngredients.includes(extraIngridient)) {
            throw new PizzaException(`Pizza does not have ingridient '${extraIngridient}'`);
        }

        this.extraIngredients = this.extraIngredients.filter((ingridient) => {
            return ingridient !== extraIngridient;            
        });

        return this.extraIngredients;
    };

    this.getExtraIngredients = () => {
        return this.extraIngredients;
    }

    this.getPizzaInfo = () => {
        return `Size: ${this.getSize()}, type: ${this.type};` +
         ` extra ingredients: ${this.getExtraIngredients()}; price: ${this.getPrice()}UAH.`;
    }
}

/* Sizes, types and extra ingredients */
Pizza.SIZE_S = 'SMALL';
Pizza.SIZE_M = 'MIDLE';
Pizza.SIZE_L = 'LARGE';

Pizza.TYPE_VEGGIE = 'VEGGIE';
Pizza.TYPE_MARGHERITA = 'MARGHERITA';
Pizza.TYPE_PEPPERONI = 'PEPPERONI';

Pizza.EXTRA_TOMATOES = 'TOMATOES';
Pizza.EXTRA_CHEESE = 'CHEESE';
Pizza.EXTRA_MEAT = 'MEAT';

Pizza.prices = {
    [Pizza.SIZE_S]: 50,
    [Pizza.SIZE_M]: 75,
    [Pizza.SIZE_L]: 100,
    [Pizza.TYPE_VEGGIE]: 50,
    [Pizza.TYPE_MARGHERITA]: 60,
    [Pizza.TYPE_PEPPERONI]: 70,
    [Pizza.EXTRA_TOMATOES]: 5,
    [Pizza.EXTRA_CHEESE]: 7,
    [Pizza.EXTRA_MEAT]: 9
};

/* Allowed properties */
Pizza.allowedSizes = [Pizza.SIZE_S, Pizza.SIZE_M, Pizza.SIZE_L];
Pizza.allowedTypes = [Pizza.TYPE_VEGGIE, Pizza.TYPE_MARGHERITA, Pizza.TYPE_PEPPERONI];
Pizza.allowedExtraIngredients = [Pizza.EXTRA_TOMATOES, Pizza.EXTRA_CHEESE, Pizza.EXTRA_MEAT];

/**
 * Provides information about an error while working with a pizza.
 * details are stored in the log property.
 * @constructor
 */
/*
function PizzaException(log) {
    return {log: log};
}
*/
class PizzaException {
    constructor(log) {
        this.log = log;
    }    
}

/* It should work 
// // small pizza, type: veggie
let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// // add extra meat
 pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// // check price
 console.log(`Price: ${pizza.getPrice()} UAH`); //=> Price: 109 UAH
// // add extra corn
 pizza.addExtraIngredient(Pizza.EXTRA_CHEESE);
// // add extra corn
 pizza.addExtraIngredient(Pizza.EXTRA_TOMATOES);
// // check price
 console.log(`Price with extra ingredients: ${pizza.getPrice()} UAH`); // Price: 121 UAH
// // check pizza size
 console.log(`Is pizza large: ${pizza.getSize() === Pizza.SIZE_L}`); //=> Is pizza large: false
// // remove extra ingredient
 pizza.removeExtraIngredient(Pizza.EXTRA_CHEESE);
 console.log(`Extra ingredients: ${pizza.getExtraIngredients().length}`); //=> Extra ingredients: 2
 console.log(pizza.getPizzaInfo()); //=> Size: SMALL, type: VEGGIE; extra ingredients: MEAT,TOMATOES; price: 114UAH.
 //pizza.removeExtraIngredient(Pizza.EXTRA_CHEESE);
// examples of errors
 let pizza1 = new Pizza(Pizza.SIZE_S); // => Required two arguments, given: 1

// let pizza1 = new Pizza(Pizza.SIZE_S, Pizza.SIZE_S); // => Invalid type

 //let pizza1 = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
 //pizza1.addExtraIngredient(Pizza.EXTRA_MEAT);
 //pizza1.addExtraIngredient(Pizza.EXTRA_MEAT); // => Duplicate ingredient

 let pizza2 = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
 pizza2.addExtraIngredient(Pizza.EXTRA_MEAT); // => Invalid ingredient
 */